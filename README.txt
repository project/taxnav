NOTE:
  This module is no longer supported.  Development efforts are now focused
on the 'navigation' module.

Overview:
---------
  The 'taxnav' module was written to provide "taxonomy-based navigation" tools.
  It currently works with the following versions of Drupal:  4.3

  There are currently two navigation tools found in taxnav:
   1) "Latest updates" blocks.  This block displays enabled vocabularies and
      terms, allowing one-click access to nodes classified under a given term.
      Additionally, it displays the date of the most recently posted node
      under this term.

   2) CSS-based navigation tabs.  The taxnav_display_tabs() function can
      be called from your theme to display CSS-based navigation tabs.

  I am interested in merging further taxonomy-based navigation tools.  If you're
working on something that you believe belongs in the taxnav module, please drop
me an email so we can work together...


Installation and configuration:
-------------------------------
  Please refer to the INSTALL file.


CSS-based navigation tabs:
--------------------------
  The CSS-based navigation tabs provide a very intuitive mechansim for
navigating a Drupal-generated website.  They are a logical extensions of the
taxonomy API.  Combined with the aliasing function, they can provide very
user-friendly navigation URLs.
  This tool provides two layers of tabs.  The top layer is generated using the
names of properly configured taonomy vocabularies.  The second layer is 
generated using the properly configured (see INSTALL file) terms within these 
vocabularies.
  When viewing nodes, no matter if you got to them using the provided tabs
or through another mechanism, the proper tabs will be highlighted to show
where you currently are in the site's organization.


See it in action:
-----------------
  The 'taxnav' module can be seen in action at the following sites:
    - http://kerneltrap.org/  (tabs & block)

  (If you are running taxnav successfully on your site, please let me know
   so that I can add you to this section.)


Cautions:
---------
  Use this module at your own risk.  I wrote it in a single afternoon, and in
the interest of finishing quickly was forced to take many short cuts when 
writing the code. It was desgined very much with my personal website,
KernelTrap, in mind, and may not work as intended on your site.

  That said, I am very interested in cleaning it up and making it more generic
so that it can be used by anyone.  If you decide to give it a try and run into
problems, please drop me an email (see 'Maintainer' section below).  Better
yet, if you have a better way to do things, please send me a patch!  :)


Maintainer:
-----------
  I no longer maintain this module, moving on to work on the 'navigation'
module which also provides CSS tabs.

  If you wish to maintain this module, please do...
